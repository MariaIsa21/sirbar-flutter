class Sale{
  late int saleid , sellerid , customerid , totalprice ;
  late String createdate , modificationdate;


  Sale({
    this.saleid = 0,
    this.sellerid = 0,
    this.customerid = 0,
    this.totalprice = 0,
    this.createdate = "",
    this.modificationdate = "",

  });


  factory Sale.fromJson(Map<String, dynamic> parsedJson) => Sale(
    saleid: parsedJson['sale_id'],
    sellerid: parsedJson['seller_id'],
    customerid: parsedJson['customer_id'],
    totalprice: parsedJson['total_price'],
    createdate: parsedJson['create_date'],
    modificationdate: parsedJson['modification_date'],


  );


  Map<String, dynamic> toJson() => {
    'sale_id': saleid,
    'seller_id': sellerid,
    'customer_id': customerid,
    'total_price': totalprice,
    'create_date': createdate,
    'modification_date': modificationdate,

  };


  Map<String, dynamic> toJsonRegistry() => {
    'sale_id': saleid,
  };




}