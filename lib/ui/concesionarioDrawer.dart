import 'package:backconcesionario_f/ui/registrarClientes.dart';
import 'package:flutter/material.dart';

class ConcesionarioDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ClienteDrawerState();
  }

}

class ClienteDrawerState extends State<ConcesionarioDrawer>{
  final minimumPadding = 5.0;
  @override
  Widget build(BuildContext context) {
  return Scaffold(
    appBar: AppBar(
      title:  Text('Menu de Servicios'),
    ),
    body: Center(child: Container(
      height: 100,
      width: 900,
      decoration:
      BoxDecoration(border: Border.all(color: Colors.white)),
      child: Text(
        "APLICATIVO SIRBAR", textAlign:  TextAlign.center,
        style: TextStyle(fontSize: 80, color: Colors.black12),
        textScaleFactor: 1,
      ),
    ),),
    drawer: Drawer(
      child: ListView(
        padding: EdgeInsets.only(top: minimumPadding , bottom: minimumPadding),
        children: <Widget>[
          DrawerHeader(
              child: Text(
                "Menu de Servicios", textAlign:  TextAlign.center,
                style: TextStyle(fontSize: 20, color: Colors.white),
                textScaleFactor: 1,
              ),
            decoration: BoxDecoration(
              color: Colors.green,
            ),
                ),
          ListTile(
            title: Text('Registro de Clientes'),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrarClientes()));
            },
          ),
          ListTile(
            title: Text('Ver Clientes'),
            onTap: (){
            },
          ),

          ListTile(
            title: Text('Registro de Clientes'),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrarClientes()));
            },
          ),
          ListTile(
            title: Text('Ver Productos'),
            onTap: (){
            },
          ),
          ListTile(
            title: Text('Registro de Productos'),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrarClientes()));
            },
          ),

          ListTile(
            title: Text('Ver Facturas'),
            onTap: (){
            },
          ),

          ListTile(
            title: Text('Registro de Facturas'),
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context) => RegistrarClientes()));
            },
          ),

        ],
      ),
    ),
  );

  }

}