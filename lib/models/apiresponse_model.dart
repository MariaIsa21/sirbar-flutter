class ApiResponse {
  late int statusResponse;
  late String message;
  late Object object;

  ApiResponse({
    this.statusResponse = 0,
    this.object = '',
    this.message = '',
  });

  factory ApiResponse.fromJson(Map<String, dynamic> parsedJson) => ApiResponse(
    statusResponse: parsedJson['statusResponse'],
    object: parsedJson['object'],
  );

  Map<String, dynamic> toJson() => {
    'statusResponse': statusResponse,
    'object': object,
  };
}
