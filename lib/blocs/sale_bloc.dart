
import 'dart:async';

import 'package:backconcesionario_f/models/apiresponse_model.dart';

import 'package:backconcesionario_f/models/sale_model.dart';
import 'package:backconcesionario_f/repository/Back_repository.dart';
import 'package:flutter/cupertino.dart';

class SaleBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _salecontroller = StreamController<Sale>();

  Stream<Sale> get product => _salecontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  SaleBloc(BuildContext context){
    _context = context ;
  }

  Future listarsale() async{
    var apiResponse = await _repository.todoslosSale();
  }


  Future listarSaleID(int id) async{
    var apiResponse = await _repository.saleporId(id);
  }

  Future insertarSale(Sale sale) async{
    var apiResponse = await _repository.insertarSale(sale);

  }

  Future borrarSale(int id) async{
    var apiResponse = await _repository.borrarSale(id);

  }



}