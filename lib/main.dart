import 'package:flutter/material.dart';

import 'ui/concesionarioDrawer.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Menu Principal', debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch:  Colors.green,
      ),
      home: ConcesionarioDrawer(),
    );
  }
}
