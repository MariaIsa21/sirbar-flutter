import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class RegistrarClientes extends  StatefulWidget{
  @override
  State<StatefulWidget> createState() {
   return RegistrarClientesState();
  }


}


class RegistrarClientesState extends State<RegistrarClientes> {
  final minimumPadding = 5.0;
  TextEditingController firstcontroller = TextEditingController();
  TextEditingController lastcontroller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    TextStyle? textStyle = Theme.of(context).textTheme.subtitle2;
     return Scaffold(
       appBar: AppBar(
         title: Text("Registrar Clientes"),
       ),
    body: Form(
      child: Padding(
      padding:  EdgeInsets.all(minimumPadding * 2),
        child:  ListView(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
            child: TextFormField(
              style: textStyle,
              controller: firstcontroller,
           //aqui va el si que valida el value vacio mirar video
              decoration: InputDecoration(
                labelText: 'Numero de Documento',
                hintText: 'Ingrese su Numero de Documento',
                labelStyle: textStyle,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0)
                )
              ),

            )
            ),

            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: lastcontroller,

                  //aqui va el si que valida el value vacio mirar video

                  decoration: InputDecoration(
                      labelText: 'Tipo de Documento ',
                      hintText: 'Ingrese Tipo de Documento',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      )
                  ),

                )
            ),

            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: lastcontroller,

                  //aqui va el si que valida el value vacio mirar video

                  decoration: InputDecoration(
                      labelText: 'Nombre Cliente ',
                      hintText: 'Ingrese Nombre del Cliente',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      )
                  ),

                )
            ),

            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: lastcontroller,

                  //aqui va el si que valida el value vacio mirar video

                  decoration: InputDecoration(
                      labelText: 'Celular ',
                      hintText: 'Ingrese Celular',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      )
                  ),

                )
            ),

            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: lastcontroller,

                  //aqui va el si que valida el value vacio mirar video

                  decoration: InputDecoration(
                      labelText: 'Direccion ',
                      hintText: 'Ingrese Dir Cliente',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      )
                  ),

                )
            ),

            Padding(padding: EdgeInsets.only(top: minimumPadding,bottom: minimumPadding),
                child: TextFormField(
                  style: textStyle,
                  controller: lastcontroller,

                  //aqui va el si que valida el value vacio mirar video

                  decoration: InputDecoration(
                      labelText: 'Ciudad ',
                      hintText: 'Ingrese Ciudad',
                      labelStyle: textStyle,
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)
                      )
                  ),

                )
            ),
            RaisedButton(
              child: Text('Enviar'),
                onPressed: (){})
          ],
        ),
      ),
     ),
     );
  }


}