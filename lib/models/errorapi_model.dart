class ErrorApiResponse {
  late int status;
  late String timestamp, error, message, path;

  ErrorApiResponse({
    status,
    timestamp,
    error,
    message,
    path,
  });
  factory ErrorApiResponse.fromJson(Map<String, dynamic> parsedJson) =>
      ErrorApiResponse(
        status: parsedJson['status'],
        timestamp: parsedJson['timestamp'],
        error: parsedJson['error'],
        message: parsedJson['message'],
        path: parsedJson['path'],
      );

  factory ErrorApiResponse.fromJsonFiles(Map<String, dynamic> parsedJson) =>
      ErrorApiResponse(
        status: 500,
        message: parsedJson['data'],
      );

  Map<String, dynamic> toJson() => {
    'status': status,
    'timestamp': timestamp,
    'error': error,
    'message': message,
    'path': path,
  };
}
