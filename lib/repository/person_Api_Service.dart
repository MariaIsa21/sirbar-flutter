import 'dart:convert';
import 'dart:io';
import 'package:backconcesionario_f/models/person_model.dart';
import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/errorapi_model.dart';
import 'package:backconcesionario_f/resource/Constants.dart';
import 'package:http/http.dart' as http;

class PersonApiService {
  late Person _person;
  late ErrorApiResponse _error;
  PersonApiService();

  Future<ApiResponse> getAllPerson() async {
    List<Person> listPerson = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllPerson);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listPerson.add(Person.fromJson(i));
        return i;
      });
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listByIdPerson(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdPerson + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _person = Person.fromJson(resBody);
      apiResponse.object = _person;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertPerson(Person person) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(person.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertPerson);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _person = Person.fromJson(resBody);
      apiResponse.object = _person;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deletePerson(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(person.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeletePerson);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Person get person => _person;
}
