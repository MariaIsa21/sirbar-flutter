class Post {
  late int userId;
  late int id;
  late String title;
  late String body;
  Post({userId, id, title, body});
  factory Post.fromJson(Map<String, dynamic> parsedJson) => Post(
    userId: parsedJson['userId'],
    id: parsedJson['id'],
    title: parsedJson['title'],
    body: parsedJson['body'],
  );
}
