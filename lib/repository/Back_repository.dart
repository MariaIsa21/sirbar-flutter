import 'package:backconcesionario_f/models/person_model.dart';
import 'package:backconcesionario_f/models/product_model.dart';
import 'package:backconcesionario_f/models/sale_model.dart';
import 'package:backconcesionario_f/models/sale_product.dart';
import 'package:backconcesionario_f/repository/person_Api_Service.dart';
import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/repository/product_Api_Service.dart';
import 'package:backconcesionario_f/repository/sale_Api_Service.dart';
import 'package:backconcesionario_f/repository/saleproduct_Api_Service.dart';


class Backrepository {
  final personApiService = PersonApiService();
  final productApiService = ProductApiService();
  final saleApiService = SaleApiService();
  final saleproductApiService = SaleproductApiService();

  ///Cliente

  Future<ApiResponse> todoslosClientes() => personApiService.getAllPerson();

  Future<ApiResponse> clienteporid(int id) =>
      personApiService.listByIdPerson(id);

  Future<ApiResponse> insertarCliente(Person person) =>
      personApiService.insertPerson(person);

  Future<ApiResponse> borrarCliente(int id) =>
      personApiService.deletePerson(id);

  ///Product

  Future<ApiResponse> todoslosproductos() => productApiService.getAllProduct();

  Future<ApiResponse> productporID(int id) =>
      productApiService.listByIdProduct(id);

  Future<ApiResponse> insertarProduct(Product product) =>
      productApiService.insertProduct(product);

  Future<ApiResponse> borrarProduct(int id) => productApiService.deleteProduct(id);

  ///Sale

  Future<ApiResponse> todoslosSale() => saleApiService.getAllSale();

  Future<ApiResponse> saleporId(int id) => saleApiService.listByIdSale(id);

  Future<ApiResponse> insertarSale(Sale sale) =>
      saleApiService.insertSale(sale);

  Future<ApiResponse> borrarSale(int id) => saleApiService.deleteSale(id);

  ///Sale product

  Future<ApiResponse> todoslosSaleproduct() =>
      saleproductApiService.getAllSaleproduct();

  Future<ApiResponse> saleproductporId(int id) =>
      saleproductApiService.listByIdSaleproduct(id);

  Future<ApiResponse> insertarSaleproduct(Saleproduct saleproduct) =>
      saleproductApiService.insertSaleproduct(saleproduct);

  Future<ApiResponse> borrarSaleproduct(int id) =>
      saleproductApiService.deleteSaleproduct(id);
}
