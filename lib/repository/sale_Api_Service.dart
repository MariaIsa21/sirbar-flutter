import 'dart:convert';
import 'dart:io';

import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/errorapi_model.dart';
import 'package:backconcesionario_f/models/sale_model.dart';
import 'package:backconcesionario_f/resource/Constants.dart';
import 'package:http/http.dart' as http;

class SaleApiService {
  late Sale _sale;
  late ErrorApiResponse _error;
  SaleApiService();

  Future<ApiResponse> getAllSale() async {
    List<Sale> listSale = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllSales);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listSale.add(Sale.fromJson(i));
        return i;
      });
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listByIdSale(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdSale + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _sale = Sale.fromJson(resBody);
      apiResponse.object = _sale;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertSale(Sale sale) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(sale.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertSale);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _sale = Sale.fromJson(resBody);
      apiResponse.object = _sale;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteSale(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(sale.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteSale);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Sale get sale => _sale;
}
