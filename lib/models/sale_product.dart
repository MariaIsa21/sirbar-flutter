class Saleproduct{
  late int saleproductid , saleid , productid , quantity;


  Saleproduct({
    this.saleproductid = 0,
    this.saleid = 0,
    this.productid = 0,
    this.quantity = 0,

  });


  factory Saleproduct.fromJson(Map<String, dynamic> parsedJson) => Saleproduct(
    saleproductid: parsedJson['sale_product_id'],
    saleid: parsedJson['sale_id'],
    productid: parsedJson['product_id'],
    quantity: parsedJson['quantity'],

  );


  Map<String, dynamic> toJson() => {
    'sale_product_id': saleproductid,
    'sale_id': saleid,
    'product_id': productid,
    'quantity': quantity,

  };


  Map<String, dynamic> toJsonRegistry() => {
    'product_id': saleproductid,
  };





}