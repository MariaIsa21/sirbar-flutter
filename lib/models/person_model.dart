class Person{
late int personid ,  documenttypeid, persontypeid  ;
late String name , emailaddress , phonenumber , city , birthdate,address,createdate , modificationdate;
late bool isSeller;



Person({
  this.personid = 0,
  this.name = "",
  this.documenttypeid = 0,
  this.persontypeid = 0,
  this.isSeller = false,
  this.birthdate = "",
  this.emailaddress = "",
  this.phonenumber = "",
  this.city = "" ,
  this.address = "" ,
  this.createdate = "",
  this.modificationdate = "",
});

factory Person.fromJson(Map<String, dynamic> parsedJson) => Person(
  personid: parsedJson['person_id'],
  name: parsedJson['name'],
  documenttypeid: parsedJson['document_type_id'],
  persontypeid: parsedJson['person_type_id'],
  isSeller: parsedJson['isSeller'],
  birthdate: parsedJson['birthdate'],
  emailaddress: parsedJson['email_address'],
  phonenumber : parsedJson['phone_number'],
  city: parsedJson['city'],
  address: parsedJson['address'],
  createdate: parsedJson['create_date'],
  modificationdate: parsedJson['modification_date'],
);


Map<String, dynamic> toJson() => {
  'person_id': personid,
  'name': name,
  'document_type_id': documenttypeid,
  'person_type_id': persontypeid,
  'isSeller': isSeller,
  'birthdate': birthdate,
  'email_address': emailaddress,
  'phone_number': phonenumber,
  'city': city,
  'address': address,
  'create_date': createdate,
  'modification_date': modificationdate,
};

Map<String, dynamic> toJsonRegistry() => {
  'person_id': personid,
};


}