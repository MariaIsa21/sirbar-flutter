
import 'dart:async';

import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/product_model.dart';
import 'package:backconcesionario_f/repository/Back_repository.dart';
import 'package:flutter/cupertino.dart';

class ProductBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _productcontroller = StreamController<Product>();

  Stream<Product> get product => _productcontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  ProductBloc(BuildContext context){
    _context = context ;
  }

  Future listarproduct() async{
    var apiResponse = await _repository.todoslosproductos();
  }


  Future listarProductID(int id) async{
    var apiResponse = await _repository.productporID(id);
  }

  Future insertarProduct(Product product) async{
    var apiResponse = await _repository.insertarProduct(product);

  }

  Future borrarProduct(int id) async{
    var apiResponse = await _repository.borrarProduct(id);

  }



}