import 'dart:convert';
import 'dart:io';

import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/errorapi_model.dart';
import 'package:backconcesionario_f/models/sale_product.dart';
import 'package:backconcesionario_f/resource/Constants.dart';
import 'package:http/http.dart' as http;

class SaleproductApiService {
  late Saleproduct _saleproduct;
  late ErrorApiResponse _error;
  SaleproductApiService();

  Future<ApiResponse> getAllSaleproduct() async {
    List<Saleproduct> listSaleproduct = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllSale_product);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listSaleproduct.add(Saleproduct.fromJson(i));
        return i;
      });
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listByIdSaleproduct(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdSale_product + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _saleproduct = Saleproduct.fromJson(resBody);
      apiResponse.object = _saleproduct;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertSaleproduct(Saleproduct saleproduct) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(saleproduct.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertSale);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _saleproduct = Saleproduct.fromJson(resBody);
      apiResponse.object = _saleproduct;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteSaleproduct(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(saleproduct.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteSale);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Saleproduct get saleproduct => _saleproduct;
}
