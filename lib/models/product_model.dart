class Product {
late int productid , quantity;
late String descripcion;
late double price;

Product({
  this.productid = 0,
  this.descripcion = "",
  this.quantity = 0,
  this.price = 0.0,

});


factory Product.fromJson(Map<String, dynamic> parsedJson) => Product(
  productid: parsedJson['product_id'],
  descripcion: parsedJson['descripcion'],
  quantity: parsedJson['quantity'],
  price: parsedJson['price'],

);


Map<String, dynamic> toJson() => {
  'product_id': productid,
  'descripcion': descripcion,
  'quantity': quantity,
  'price': price,

};


Map<String, dynamic> toJsonRegistry() => {
  'product_id': productid,
};




}