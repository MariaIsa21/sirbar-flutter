import 'dart:convert';
import 'dart:io';
import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/errorapi_model.dart';
import 'package:backconcesionario_f/models/product_model.dart';
import 'package:backconcesionario_f/resource/Constants.dart';
import 'package:http/http.dart' as http;

class ProductApiService {
  late Product _product;
  late ErrorApiResponse _error;
  ProductApiService();

  Future<ApiResponse> getAllProduct() async {
    List<Product> listProduct = [];
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlFindAllProduct);
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      resBody.forEach((i) {
        listProduct.add(Product.fromJson(i));
        return i;
      });
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> listByIdProduct(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    Uri url = Uri.http(
      Constants.urlAuthority,
      Constants.urlListByIdProduct + id.toString(),
    );
    var res = await http.get(
      url,
      headers: {HttpHeaders.authorizationHeader: Constants.authorizationHeader},
    );
    var resBody = json.decode(res.body);

    apiResponse.statusResponse = res.statusCode;

    if (apiResponse.statusResponse == 200) {
      _product = Product.fromJson(resBody);
      apiResponse.object = _product;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> insertProduct(Product product) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(product.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlInsertProduct);
    var res = await http.post(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      _product = Product.fromJson(resBody);
      apiResponse.object = _product;
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Future<ApiResponse> deleteProduct(int id) async {
    ApiResponse apiResponse = ApiResponse(statusResponse: 0);
    var body = json.encode(product.toJson());
    Uri url = Uri.http(Constants.urlAuthority, Constants.urlDeleteProduct);
    var res = await http.delete(
      url,
      headers: {
        HttpHeaders.contentTypeHeader: Constants.contentTypeHeader,
      },
      body: body,
    );

    var resBody = json.decode(res.body);
    apiResponse.statusResponse = res.statusCode;
    if (apiResponse.statusResponse == 200) {
      apiResponse.message = resBody.toString();
    } else {
      _error = ErrorApiResponse.fromJson(resBody);
      apiResponse.object = _error;
    }
    return apiResponse;
  }

  Product get product => _product;
}
