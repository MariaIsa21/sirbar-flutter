class Constants {
  static const String contentTypeHeader = 'application/json';
  static const String authorizationHeader = 'Bearer ';
  static const String urlAuthority = 'localhost:8081';
  static const String urlFindAllSales = '/api/sale';
  static const String urlInsertSale = '/api/sale';
  static const String urlDeleteSale = '/api/sale';
  static const String urlListByIdSale = '/api/sale/';
  static const String urlFindAllPerson = '/api/person';
  static const String urlListByIdPerson = '/api/person/';
  static const String urlInsertPerson = '/api/person';
  static const String urlDeletePerson = '/api/person';
  static const String urlFindAllProduct = '/api/product';
  static const String urlListByIdProduct = '/api/product/';
  static const String urlInsertProduct= '/api/product';
  static const String urlDeleteProduct = '/api/product';
  static const String urlFindAllSale_product = '/api/sale_product';
  static const String urlListByIdSale_product = '/api/sale_product/';
  static const String urlInsertSale_product = '/api/sale_product';
  static const String urlDeletePSale_product = '/api/sale_product';

}
