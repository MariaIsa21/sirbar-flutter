
import 'dart:async';

import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/person_model.dart';
import 'package:backconcesionario_f/repository/Back_repository.dart';
import 'package:flutter/cupertino.dart';

class PersonBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _personcontroller = StreamController<Person>();

  Stream<Person> get person => _personcontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  PersonBloc(BuildContext context){
    _context = context ;
  }

  Future listarPersonas() async{
    var apiResponse = await _repository.todoslosClientes();
  }


  Future listarPersonID(int id) async{
    var apiResponse = await _repository.clienteporid(id);
  }

  Future insertarPersona(Person person) async{
    var apiResponse = await _repository.insertarCliente(person);

  }

  Future borrarPersona(int id) async{
    var apiResponse = await _repository.borrarCliente(id);

  }



}