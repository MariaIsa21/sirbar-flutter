
import 'dart:async';

import 'package:backconcesionario_f/models/apiresponse_model.dart';
import 'package:backconcesionario_f/models/sale_product.dart';
import 'package:backconcesionario_f/repository/Back_repository.dart';
import 'package:flutter/cupertino.dart';

class SaleproductBloc{
  late BuildContext _context;
  final _repository = Backrepository();
  var _apiResponse = ApiResponse();
  final _saleproductcontroller = StreamController<Saleproduct>();

  Stream<Saleproduct> get product => _saleproductcontroller.stream;


  ApiResponse get apiResponse => _apiResponse;
  SaleproductBloc(BuildContext context){
    _context = context ;
  }

  Future listarsaleproduct() async{
    var apiResponse = await _repository.todoslosSaleproduct();
  }


  Future listarSaleproductID(int id) async{
    var apiResponse = await _repository.saleproductporId(id);
  }

  Future insertarSaleproduct(Saleproduct saleproduct) async{
    var apiResponse = await _repository.insertarSaleproduct(saleproduct);

  }

  Future borrarSaleproduct(int id) async{
    var apiResponse = await _repository.borrarSaleproduct(id);

  }



}